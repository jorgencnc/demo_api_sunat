package bbva.sunat.apirest.controlador;

import org.springframework.beans.factory.annotation.Value;

public class Rutas {
 public static final String BASE = "/api/v1";
 public static final String RUC = BASE + "/ruc";
 public static final String SOLICITUD = BASE + "/solicitud";
}