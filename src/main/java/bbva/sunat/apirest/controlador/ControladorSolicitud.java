package bbva.sunat.apirest.controlador;

import bbva.sunat.apirest.modelos.Ruc;
import bbva.sunat.apirest.modelos.Solicitud;
import bbva.sunat.apirest.servicios.ObjetoNoEncontrado;
import bbva.sunat.apirest.servicios.ServicioRuc;
import bbva.sunat.apirest.servicios.ServicioSolicitud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.SOLICITUD)
public class ControladorSolicitud {


    @Autowired
    ServicioSolicitud servicioSolicitud;

    @Autowired
    ServicioRuc servicioRuc;

    @Autowired
    PagedResourcesAssembler pagedResourcesAssembler;

    @GetMapping("/{codigoSolicitud}")
    public Solicitud obtenerSolicitud( @PathVariable String codigoSolicitud) {

        try {
        return  servicioSolicitud.obtenerSolicitud(codigoSolicitud);

        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping
    public ResponseEntity agregarSolcitud(@RequestBody Solicitud solicitud) {

        try {

           Ruc ruc =servicioRuc.procesarRuc(solicitud.ruc);

            if(ruc.estadoContribuyente.equals("ACTIVO")) {

            this.servicioSolicitud.insertarSolicitudNuevo(solicitud);
            final var representacionMetodoObtenerUnClienteConDocumento = methodOn(ControladorSolicitud.class)
                    .obtenerSolicitud(solicitud.codigoSolicitud);

            final var enlaceEsteDocumento = linkTo(representacionMetodoObtenerUnClienteConDocumento).toUri();

            return ResponseEntity
                    .accepted()
                    .location(enlaceEsteDocumento)
                    .body("Solicitud creada con éxito!");

            }else {
                return new ResponseEntity<>("El Ruc "+solicitud.ruc +" Estado Contribuyente : "+ruc.estadoContribuyente, HttpStatus.NOT_FOUND);

            }
        } catch(Exception x) {
            return new ResponseEntity<>("El Codigo Solicitud "+solicitud.codigoSolicitud +" ya fue registrado", HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping
    public PagedModel<EntityModel<Solicitud>> obtenerRucs(@RequestParam int pagina, @RequestParam int cantidad) {
        try {
            final var pageSinMetadatos = this.servicioSolicitud.obtenerSolicitudes(pagina, cantidad);
            final var page = pageSinMetadatos.map(
                    x -> EntityModel.of(x).add(
                            linkTo(methodOn(this.getClass()).obtenerSolicitud(x.codigoSolicitud))
                                    .withSelfRel().withTitle("solicitud")

                    ));

            return this.pagedResourcesAssembler.toModel(page);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }



}
