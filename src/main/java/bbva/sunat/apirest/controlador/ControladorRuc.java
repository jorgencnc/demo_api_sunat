package bbva.sunat.apirest.controlador;

import bbva.sunat.apirest.modelos.Ruc;
import bbva.sunat.apirest.servicios.ObjetoNoEncontrado;
import bbva.sunat.apirest.servicios.ServicioRuc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.RUC)
public class ControladorRuc {


    @Autowired
    ServicioRuc servicioRuc;

    @Autowired
    PagedResourcesAssembler pagedResourcesAssembler;

    @GetMapping("/{ruc}")
    public Ruc obtenerRuc( @PathVariable String ruc) {

        try {
        return  servicioRuc.procesarRuc(ruc);

        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping
    public PagedModel<EntityModel<Ruc>> obtenerRucs(@RequestParam int pagina, @RequestParam int cantidad) {
        try {
            final var pageSinMetadatos = this.servicioRuc.obtenerRucs(pagina, cantidad);
            final var page = pageSinMetadatos.map(
                    x -> EntityModel.of(x).add(
                            linkTo(methodOn(this.getClass()).obtenerRuc(x.ruc))
                                    .withSelfRel().withTitle("ruc")

                    ));

            return this.pagedResourcesAssembler.toModel(page);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{ruc}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity eliminarUnRuc(@PathVariable String ruc) {
        try {
            this.servicioRuc.borrarRuc(ruc);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch(Exception x) {
            return new ResponseEntity<>("Ruc no encontrado." +ruc, HttpStatus.NOT_FOUND);
        }
    }

}
