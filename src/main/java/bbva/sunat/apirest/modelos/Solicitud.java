package bbva.sunat.apirest.modelos;


import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Solicitud {

    @Id
    @JsonProperty(access= JsonProperty.Access.READ_ONLY)
    public String codigoSolicitud;
    public String ruc;
    public String estado;
    public Date fechaConstitucion;
    public String telefono;
    public String correoElectronico;
    public Date fechaSolicitud;

}
