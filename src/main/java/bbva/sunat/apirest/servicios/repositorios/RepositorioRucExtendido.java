package bbva.sunat.apirest.servicios.repositorios;

import bbva.sunat.apirest.modelos.Ruc;

public interface RepositorioRucExtendido {

    public void emparcharRuc(Ruc ruc);

}
