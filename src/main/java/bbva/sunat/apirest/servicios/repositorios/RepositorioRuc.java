package bbva.sunat.apirest.servicios.repositorios;


import bbva.sunat.apirest.modelos.Ruc;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface RepositorioRuc extends MongoRepository<Ruc, String>,
        RepositorioRucExtendido {

    public Optional<Ruc> findByRuc(String ruc);

    public void deleteByRuc(String ruc);
}

