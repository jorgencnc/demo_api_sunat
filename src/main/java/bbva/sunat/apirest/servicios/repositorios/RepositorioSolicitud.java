package bbva.sunat.apirest.servicios.repositorios;



import bbva.sunat.apirest.modelos.Solicitud;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RepositorioSolicitud extends MongoRepository<Solicitud, String>,
        RepositorioSolicitudExtendido {

    public Optional<Solicitud> findByCodigoSolicitud(String ruc);



}

