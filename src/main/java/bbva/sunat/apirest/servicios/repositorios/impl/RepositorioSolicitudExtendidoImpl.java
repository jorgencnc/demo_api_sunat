package bbva.sunat.apirest.servicios.repositorios.impl;

import bbva.sunat.apirest.modelos.Solicitud;
import bbva.sunat.apirest.servicios.repositorios.RepositorioSolicitudExtendido;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Repository
public class RepositorioSolicitudExtendidoImpl implements RepositorioSolicitudExtendido {

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public void emparcharSolicitud(Solicitud parche) {
        final Query query = query(where("_id").is(parche.codigoSolicitud));
        final Update update = new Update();

        set(update, "estado", parche.estado);


        mongoOperations.updateFirst(query, update, "solicitud");
    }

    private void set(Update update, String nombre, Object valor) {
        if(valor != null) {
            System.err.println(String.format("==== MODIFICAR CAMPO %s = %s", nombre, valor));
            update.set(nombre, valor);
        }
    }
}
