package bbva.sunat.apirest.servicios.repositorios;

import bbva.sunat.apirest.modelos.Solicitud;

public interface RepositorioSolicitudExtendido {

    public void emparcharSolicitud(Solicitud solicitud);

}
