package bbva.sunat.apirest.servicios;
import bbva.sunat.apirest.modelos.Ruc;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ServicioRuc {



    public Page<Ruc> obtenerRucs(int pagina, int cantidad);


    public Ruc obtenerRuc(String documento);

    public Ruc procesarRuc(String Ruc);
    public void emparcharRuc(Ruc parche);
    public Ruc ObtenerInformacionExternoRuc(String Ruc) ;

    public void borrarRuc(String documento);




}

