package bbva.sunat.apirest.servicios.impl;
import bbva.sunat.apirest.servicios.ServicioSolicitud;
import bbva.sunat.apirest.modelos.Solicitud;
import bbva.sunat.apirest.servicios.ObjetoNoEncontrado;
import bbva.sunat.apirest.servicios.repositorios.RepositorioSolicitud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Service
public class ServicioSolicitudImpl implements ServicioSolicitud {

    @Autowired
    RepositorioSolicitud repositorioSolicitud;


    @Value("${api.externa.v1}")
    private String urlExterna;

    @Value("${api.externa.validacion}")
    private Integer validacionDias;

    @Autowired
    RestTemplate restTemplate;

    @Override
    public Page<Solicitud> obtenerSolicitudes(int pagina, int cantidad) {
        return this.repositorioSolicitud.findAll(PageRequest.of(pagina, cantidad));
    }


    @Override
    public Solicitud obtenerSolicitud(String codigoSolicitud) {
        final Optional<Solicitud> quizasSolicitud = this.repositorioSolicitud.findByCodigoSolicitud(codigoSolicitud);

        if (!quizasSolicitud.isPresent())
            throw new ObjetoNoEncontrado("No existe Solicitud : " + codigoSolicitud);
        return quizasSolicitud.get();
    }

    @Override
    public void insertarSolicitudNuevo(Solicitud solicitud) {

        try {
            String fechaRegistroActual = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

            Date fechaCreacion=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaRegistroActual);
            solicitud.fechaSolicitud=fechaCreacion;

            this.repositorioSolicitud.insert(solicitud);
        } catch(Exception x) {
            System.out.println(x.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY);
        }

    }







}
