package bbva.sunat.apirest.servicios;


import bbva.sunat.apirest.modelos.Solicitud;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ServicioSolicitud {



    public Page<Solicitud> obtenerSolicitudes(int pagina, int cantidad);
    public Solicitud obtenerSolicitud(String codigoSolicitud);
    public void insertarSolicitudNuevo(Solicitud solicitud);




}

